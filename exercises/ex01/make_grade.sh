#!/usr/bin/env bash
# File       : make_grade.sh
# Description: Generate your exercise grade
# Copyright 2020 ETH Zurich. All Rights Reserved.
#
# EXAMPLE:
# python grade.py \
#     --question1 0 \
#     --comment1 'Add a comment to justify your score' \
#     --question2 0 \
#     --question2 0 \
#
# FOR HELP:
# python grade.py --help
#
# The script generates a grade.txt file. Submit your grade on Moodle:
# https://moodle-app2.let.ethz.ch/course/view.php?id=13666

# Note: --question1 and --question2 are not graded
python3 grade.py \
    --question1 0 \
    --comment1 'Add a comment to justify your score 1-1' \
    --comment1 'Add a comment to justify your score 1-2' \
    --question2 0 \
    --comment2 'Add a comment to justify your score 2' \
    --question3 0 \
    --comment3 'Add a comment to justify your score 3' \
