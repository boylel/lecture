// File       : mpi_coll_p2p.cpp
// Description: Collective / point-to-point mixup
// Copyright 2020 ETH Zurich. All Rights Reserved.

#include <iostream>
#include <mpi.h>
using namespace std;

/*
Questions:

1. Will it compile? : yes
2. If it compiles, what is the output when run with one process? : [0] 0; completes
3. If it compiles, what is the output when run with mpirun -n 2? : [0] 0; deadlock
4. (optional) Where does deadlock happen? : rank[0] in MPI_Finalize, rank[1] in MPI_Recv
 */

int main(int argc, char* argv[])
{
    MPI_Init(&argc, &argv);

    int rank, size;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    int bval;
    if (0 == rank)
    {
        bval = rank;
        MPI_Bcast(&bval, 1, MPI_INT, 0, MPI_COMM_WORLD);
    }
    else
    {
        MPI_Status stat;
        MPI_Recv(&bval, 1, MPI_INT, 0, rank, MPI_COMM_WORLD, &stat);
    }

    cout << "[" << rank << "] " << bval << endl;

    MPI_Finalize();
    return 0;
}
