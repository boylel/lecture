#include "simulation.h"

#include <cstdio>
#include <iomanip>
#include <sstream>

int main(int argc, char** argv)
{
    MPI_Init(&argc, &argv);

    MPI_Comm comm = MPI_COMM_WORLD;

    if (argc != 3) {
        fprintf(stderr, "usage: %s <nranks x> <nranks y>\n", argv[0]);
        MPI_Abort(MPI_COMM_WORLD, -1);
    }

    MPI_Comm_set_errhandler(comm, MPI_ERRORS_RETURN);

    const int2 nRanks2D{atoi(argv[1]), atoi(argv[2])};

    // interaction parameters
    const real epsilon = 1.0_r;
    const real sigma = 1.1224620483_r;
    const real rc = 2.5_r * sigma;
    LennardJones lj(rc, epsilon, sigma);

    const real number_density = 0.4_r;
    const real mass = 1.0_r;
    const real L = 50.0_r;
    const Domain domain{comm, real2{L, L}, nRanks2D};
    const int np = static_cast<int>(L * L * number_density);
    const real kBT = 1.0_r;

    Simulation sim(MPI_COMM_WORLD, nRanks2D, domain, lj, mass, kBT, np);

    const real dt = 5e-3_r;
    const int nsteps = 10000;

    const int reportEvery = 100;
    const int dumpEvery = 100;

    for (int i = 0; i < nsteps; ++i) {
        if (i % reportEvery == 0)
            sim.printStats(stdout);

        if (i % dumpEvery == 0) {
            std::stringstream ss;
            const int dumpId = i / dumpEvery;
            ss << "particles-" << std::setfill('0') << std::setw(5) << dumpId
               << ".csv";
            sim.dumpCsv(ss.str());
            // ss << "particles-" << std::setfill('0') << std::setw(5) << dumpId
            //    << ".xyz";
            // sim.dumpXyz(ss.str());
        }

        sim.advance(dt);
    }

    printf("simulation done\n");

    MPI_Finalize();
    return 0;
}
