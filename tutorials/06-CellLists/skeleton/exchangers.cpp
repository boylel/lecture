#include "exchangers.h"

namespace fragment_map
{

inline int getDirx(int id)
{
    return (id + 2) % 3 - 1;
}
inline int getDiry(int id)
{
    return (id / 3 + 2) % 3 - 1;
}

inline int2 getDir(int id)
{
    return {getDirx(id), getDiry(id)};
}

inline int getId(int dx, int dy)
{
    return ((dx + 2) % 3) + 3 * ((dy + 2) % 3);
}

inline int getId(int2 dir)
{
    return getId(dir.x, dir.y);
}

constexpr int bulkId = 8;

} // namespace fragment_map

BasicExchanger::BasicExchanger(MPI_Comm comm, const Domain& domain,
                               int2 nRanks2D)
    : comm(comm), domain(domain)
{
    int rank, size;
    MPI_Check(MPI_Comm_rank(comm, &rank));
    MPI_Check(MPI_Comm_size(comm, &size));

    if (size != nRanks2D.x * nRanks2D.y) {
        fprintf(stderr, "Mismatch in number of ranks\n");
        MPI_Abort(comm, -1);
    }

    const int2 rank2D{rank % nRanks2D.x, rank / nRanks2D.x};

    for (int i = 0; i < numNeighbors; ++i) {
        const int2 d = fragment_map::getDir(i);

        const int2 dstRank{(rank2D.x + d.x + nRanks2D.x) % nRanks2D.x,
                           (rank2D.y + d.y + nRanks2D.y) % nRanks2D.y};

        const int2 srcRank{(rank2D.x - d.x + nRanks2D.x) % nRanks2D.x,
                           (rank2D.y - d.y + nRanks2D.y) % nRanks2D.y};

        dstRanks[i] = dstRank.x + nRanks2D.x * dstRank.y;
        srcRanks[i] = srcRank.x + nRanks2D.x * srcRank.y;
    }
}

void BasicExchanger::send()
{
    for (size_t i = 0; i < numNeighbors; ++i) {
        const auto& buffer = sendBuffers[i];
        sendSizes[i] = buffer.size();

        const int dest = dstRanks[i];
        const int tag = i;

        MPI_Check(MPI_Isend(&sendSizes[i], 1, MPI_INT, dest, tag, comm,
                            &sendSizeReqs[i]));

        MPI_Check(MPI_Isend(buffer.data(), nRealPerParticle * buffer.size(),
                            getMPIFloatType<real>(), dest, tag, comm,
                            &sendDataReqs[i]));
    }
}

void BasicExchanger::recv()
{
    // recv sizes
    for (size_t i = 0; i < numNeighbors; ++i) {
        const int src = srcRanks[i];
        const int tag = i;
        MPI_Check(MPI_Irecv(&recvSizes[i], 1, MPI_INT, src, tag, comm,
                            &recvSizeReqs[i]));
    }

    MPI_Check(
        MPI_Waitall(numNeighbors, recvSizeReqs.data(), MPI_STATUSES_IGNORE));

    // recv data
    for (size_t i = 0; i < numNeighbors; ++i) {
        auto& buffer = recvBuffers[i];
        const int src = srcRanks[i];
        const int tag = i;

        buffer.resize(recvSizes[i]);

        MPI_Check(MPI_Irecv(buffer.data(), nRealPerParticle * buffer.size(),
                            getMPIFloatType<real>(), src, tag, comm,
                            &recvDataReqs[i]));
    }

    MPI_Check(
        MPI_Waitall(numNeighbors, recvDataReqs.data(), MPI_STATUSES_IGNORE));

    // wait for send requests
    MPI_Check(
        MPI_Waitall(numNeighbors, sendSizeReqs.data(), MPI_STATUSES_IGNORE));
    MPI_Check(
        MPI_Waitall(numNeighbors, sendDataReqs.data(), MPI_STATUSES_IGNORE));
}

Redistributor::Redistributor(MPI_Comm comm, const Domain& domain, int2 nRanks2D) :
    BasicExchanger(comm, domain, nRanks2D)
{}

void Redistributor::redistribute(std::vector<Particle>& particles)
{
    shiftAndPack(particles);
    send();
    recv();
    unpack(particles);
}

void Redistributor::shiftAndPack(const std::vector<Particle>& particles)
{
    bulk.reserve(particles.size());
    bulk.resize(0);

    for (auto& b : sendBuffers)
        b.resize(0);

    const auto L = domain.localSize;

    for (auto p : particles) {
        const int2 dir{(p.r.x >= -0.5_r * L.x) + (p.r.x >= 0.5_r * L.x) - 1,
                       (p.r.y >= -0.5_r * L.y) + (p.r.y >= 0.5_r * L.y) - 1};

        const int dstId = fragment_map::getId(dir);

        if (dstId == fragment_map::bulkId) {
            bulk.push_back(p);
        } else {
            // shift
            p.r.x -= L.x * dir.x;
            p.r.y -= L.y * dir.y;
            sendBuffers[dstId].push_back(p);
        }
    }
}

void Redistributor::unpack(std::vector<Particle>& particles)
{
    std::swap(particles, bulk);
    for (const auto& buffer : recvBuffers)
    {
        for (auto p : buffer)
            particles.push_back(p);
    }
}


GhostExchanger::GhostExchanger(MPI_Comm comm, const Domain& domain, int2 nRanks2D) :
    BasicExchanger(comm, domain, nRanks2D)
{}

void GhostExchanger::exchangeGhosts(const CellListsView& cl, const std::vector<Particle>& particles, std::vector<Particle>& ghosts)
{
    shiftAndPack(cl, particles);
    send();
    recv();
    unpack(ghosts);
}

void GhostExchanger::shiftAndPack(const CellListsView& cl,
                                  const std::vector<Particle>& particles)
{
    const auto L = domain.localSize;

    for (int buffId = 0; buffId < numNeighbors; ++buffId) {
        auto& buffer = sendBuffers[buffId];
        buffer.resize(0);

        const int2 d = fragment_map::getDir(buffId);

        // choose all cells in the direction d

        const int minCidx = d.x <= 0 ? 0 : cl.nCells.x - 1;
        const int minCidy = d.y <= 0 ? 0 : cl.nCells.y - 1;

        const int maxCidx = d.x >= 0 ? cl.nCells.x - 1 : 0;
        const int maxCidy = d.y >= 0 ? cl.nCells.y - 1 : 0;

        for (int cidy = minCidy; cidy <= maxCidy; ++cidy) {
            for (int cidx = minCidx; cidx <= maxCidx; ++cidx) {
                const int cid = cl.toLinearCid({cidx, cidy});
                const int start = cl.cellStarts[cid];
                const int end = cl.cellStarts[cid + 1];

                for (int i = start; i < end; ++i) {
                    auto p = particles[i];
                    // shift
                    p.r.x -= L.x * d.x;
                    p.r.y -= L.y * d.y;
                    buffer.push_back(p);
                }
            }
        }
    }
}

void GhostExchanger::unpack(std::vector<Particle>& ghosts)
{
    ghosts.resize(0);

    for (const auto& buffer : recvBuffers) {
        for (auto p : buffer)
            ghosts.push_back(p);
    }
}
