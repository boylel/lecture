#ifndef SIMPLECLASS_HPP
#define SIMPLECLASS_HPP

#include <stddef.h>

class SimpleClass
{
  public:
    // constructors
    SimpleClass(int value);
    SimpleClass();

    // destructor
    ~SimpleClass();

    // member functions
    void set_member(int value);

    int get_member();

    // static functions
    static size_t get_object_counter();

  private:
    // a member variable
    int x_;

    // a static variable
    static size_t obj_counter_;
};

#endif // SIMPLECLASS_HPP
